const fs = require('fs');
const pathNode = require('path');
const MidiWriter = require('midi-writer-js');
const Tonal = require('tonal');
const gaussian = require('gaussian');

const debug = {
    notes: {
        /** Debug bass note generation */
        bass: false,
        soprano: {
            generation: false,
            /** Debug soprano gaussian distribution increment generation.  */
            gaussianDistribution: false,
        },
        /** Whether the final list of notes should be printed. */
        output: false,
    },
    /** Debug the process of generating chords. */
    chords: false,
};

// TODO: Add 'Note' typedef and replace all references where I just put 'number'. E.g. 'number[]' should be 'Note[]'

/**
 * @typedef {string[]} NoteNames
 */

/**
 * @typedef {Object} StackedNotes
 * @property {number[]} bassNotes 
 * @property {number[]} sopranoNotes
 * @property {number[]} thirdVoice
 */

/**
 * @typedef {string} SongKey The key of the song. For example, 'C'
 */

/**
 * @typedef {string} Chord
 */

/**
 * @typedef {number} SopranoDistance Distance from bass notes to soprano notes.
 */

class Song {
    /**
     * 
     * @param {SongKey} key The key of the song. For example, 'C'
     * @param {number} firstNote
     * @param {number} totalNotes The total number of notes in the song.
     * @param {number} sopranoDistance 
     * @param {string} noteDuration The duration of each note
     */
    constructor(key, firstNote, totalNotes, sopranoDistance, noteDuration) {
        /** @type {SongKey} The key of the song. For example, 'C' */
        this.key = key;
        this.firstNote = firstNote;
        /** @type {number} The total number of notes in the song. */
        this.totalNotes = totalNotes;
        /** @type {SopranoDistance} Distance from bass notes to soprano notes. */
        this.sopranoDistance = sopranoDistance;
        /** @type {string} The duration of each note */
        this.noteDuration = noteDuration;

        /** @type {NoteNames} */
        this.noteNames = [];
        const scale = Tonal.Scale.notes(key, 'major');
        for (let i=0;i<5;i++) {
            for (const note of scale) {
                this.noteNames.push(`${note}${i+2}`);
            }
        }

        /** @type {StackedNotes} */
        this.stackedNotes = {
            bassNotes: [],
            sopranoNotes: [],
            thirdVoice: [],
        };

        this.progressionChords = [];

        this.noteEvents = [];
    }

    generate() {
        this.stackedNotes.bassNotes = generateBassNotes(this, this.firstNote);
        this.stackedNotes.sopranoNotes = generateSopranoNotes(this);
        this.progressionChords = generateProgressionChords(this);
        this.stackedNotes.thirdVoice = generateThirdVoice(this);
        this.noteEvents = generateNoteEvents(this, this.noteDuration);
    }

    createMidiTrack() {
        createMidiTrack(this.noteEvents);
    }
}

//#region Utils

/**
 * Tests a random value generator and returns the percentage chance of each value.
 * @param {function} random The random value generator.
 * @return {Object.<any, number>} Object containing the percentage chance of each possible random value.
 */
function testRandomValueGenerator(random, tries=1000) {
    const count = {};
    for (let i=0;i<tries;i++) {
        const increment = random();
        if (increment in count) {
            count[increment] += 1;
        } else {
            count[increment] = 0;
        }
    }
    const percentages = {};
    for (const value of Object.keys(count)) {
        percentages[value] = Math.round((count[value] / tries) * 100);
    }
    return percentages;
}

/**
 * Randomly selects an item from an array.
 * @param {Array} array The array to randomly choose items from.
 * @return {any}
 */
function randomItemInArray(array) {
    return array[Math.floor(Math.random() * array.length)];
}

/**
 * Matrix of possible outcomes and their corresponding percentage probabilities as decimals.
 * @typedef {Object.<number,number>} ProbabilityMatrix
 */

/**
 * Randomly chooses an increment bassd on the provided probability matrix.
 * @param {ProbabilityMatrix} matrix
 * @return {function}
 */
// TODO: Clean me up
function randomIncrementGenerator(matrix) {
    let i, j, table=[];
    for (i in matrix) {
      // The constant 1000 below should be computed bassd on the
      // weights in the spec for a correct and optimal table size.
      for (j=0; j<matrix[i]*1000; j++) {
        table.push(Number(i));
      }
    }
    return function () {
        return randomItemInArray(table);
    };
}

/**
 * If the interval between the previous and current note is a tritone, 
 * fix it by making the current note closer to the previous one
 * 
 * TODO: Name me better
 * @param {string[]} noteNames
 * @param {number} previousNote 
 * @param {number} currentNote 
 */
function correctTritone(noteNames, previousNote, currentNote) {
    let distance = Tonal.Distance.semitones(noteNames[previousNote], noteNames[currentNote]);
    const distanceRemainder = distance % 12;
    if (distanceRemainder === 6) {
        return currentNote - 1;
    } else if (distanceRemainder === -6) {
        return currentNote + 1;
    } else {
        return currentNote;
    }
}

function writeCadence(voice, notes) {
    let firstNote = notes[0];
    let fourthLast = notes[notes.length-4];
    let fifthLast = notes[notes.length-5]
    let lastNote = firstNote;
    let secondLast = null;
    if (voice === 'bass') {
        if (fourthLast <= lastNote) {
            secondLast = lastNote - 3;
        } else {
            secondLast = lastNote + 4;
        }
    } else if (voice === 'soprano') {
        secondLast = lastNote - 1;
    }
    let thirdLast = null;
    const diff = secondLast - fourthLast;
    // Make sure there are no repeating notes and no leaps followed by steps in the same direction
    if (Math.abs(diff) <= 1) {
        // if fifth last is above second and fourth
        if (fifthLast > secondLast && fifthLast > fourthLast) {
            // one step above the higher one
            if (secondLast > fourthLast) {
                thirdLast = secondLast + 1;
            } else {
                thirdLast = fourthLast + 1;
            }
        // if fifth last is below second and fourth
        } else if (fifthLast < secondLast && fifthLast < fourthLast) {
            // one step below the higher one
            if (secondLast < fourthLast) {
                thirdLast = secondLast - 1;
            } else {
                thirdLast = fourthLast - 1;
            }
        } else if (fifthLast === secondLast) {
            if (fourthLast > secondLast) {
                thirdLast = secondLast - 1;
            } else {
                thirdLast = secondLast + 1;
            }
        }
    } else {
        thirdLast = Math.ceil((fourthLast + secondLast)/2);
    }
    return [thirdLast, secondLast, lastNote]
}

//#endregion

//#region Bass notes

// TODO: Document me
function calculateBassNote(previousNote, currentDirection, interval) {
    return previousNote + currentDirection * (interval-1);
}

/**
 * TODO: Describe me
 * @param {Song} song
 * @param {number} firstNote The first bass note.
 * @param {ProbabilityMatrix} randomMatrix 
 * @return {number[]} Notes
 */
function generateBassNotes({noteNames, totalNotes}, firstNote, randomMatrix={ 2: 0.5, 3: 0.3, 4: 0.1, 5: 0.1 }) {
    let currentDirection = 1;
    let lastInterval = null;
    // TODO: Make sure you use this right
    let previousNote = firstNote;

    if (debug.notes.bass) {
        console.group('Bass note generation');
        console.log(`Starting note is ${noteNames[firstNote]} (${firstNote})`);
    }

    const notes = [firstNote];
    const getRandomBassIncrement = randomIncrementGenerator(randomMatrix);

    for (let i=1;i<totalNotes-3;i++) {
        if (debug.notes.bass) {
            console.log('=============');
            console.log('position : ', i);
        }

        let interval = null;

        //#region Choose next direction and interval
        const changeDirectionRandom = Math.random();
        // if change is greater than or equal to 4, change direction
        // if not, randomly change direction 35% of the time
        if (lastInterval >= 4 || (changeDirectionRandom < 0.35)) {
            currentDirection *= -1;
            if (lastInterval >= 4) {
                interval = 2;
                if (debug.notes.bass) {
                    console.log(`The last interval ${lastInterval} is >= 4, so change direction`);
                    console.log(`Also set the interval to 2`);
                }
            } else if (debug.notes.bass) {
                console.log(`Randomly changed direction`);
            }
        }

        if (interval === null) {
            interval = getRandomBassIncrement();
            if (debug.notes.bass) {
                console.log(`Generated random interval of ${interval}`);
            }
        }
        //#endregion
        
        let currentNote = calculateBassNote(previousNote, currentDirection, interval);
        if (debug.notes.bass) {
            console.log(`Previous note was ${noteNames[previousNote]}`);
            const directionNames = {'1': 'up', '-1': 'down'}
            console.log(`Current direction is ${directionNames[currentDirection]}`);
            let lastBit = '';
            if (currentDirection > 0) {
                lastBit = ` + ${interval-1}`;
            } else {
                lastBit = ` - ${interval-1}`;
            }
            console.log(`Therefore, next note is ${noteNames[previousNote]}${lastBit} = ${noteNames[currentNote]}`);
        }

        // if current note is at end of range, change it to be the other direction
        if (currentNote >= firstNote + 8 || currentNote <= firstNote - 8) {
            currentDirection *= -1;
            currentNote = calculateBassNote(previousNote, currentDirection, interval);
            if (debug.notes.bass) {
                console.log(`However, since that note is further than an octave away from the first note (${noteNames[firstNote]}), change direction and make the next note ${noteNames[currentNote]} instead`);
            }
        }

        // fix tritone issues
        let whatCurrentNoteWas = currentNote;
        currentNote = correctTritone(noteNames, previousNote, currentNote);
        if (debug.notes.bass && currentNote !== whatCurrentNoteWas) {
            console.log(`Since the difference between the previous note (${noteNames[previousNote]}) and the next note (${noteNames[whatCurrentNoteWas]}) is a tritone, the next should be ${noteNames[currentNote]}`);
        }

        notes.push(currentNote);
        previousNote = currentNote;
        lastInterval = interval;
    }

    notes.push(...writeCadence('bass', notes));

    if (debug.notes.bass) {
        console.groupEnd();
    }

    return notes;
}

//#endregion

//#region Soprano notes

/**
 * Calculates a soprano note from a bass note, an increment and the soprano distance.
 * @param {number} bassNote 
 * @param {number} increment 
 * @param {SopranoDistance} sopranoDistance 
 * @return {number}
 * @example 
 * calculateSopranoNote(7, 3, 14) // => 24
 */
function calculateSopranoNote(bassNote, increment, sopranoDistance) {
    return bassNote + sopranoDistance + increment;
}

const gaussianDistribution = gaussian(0, 3);

/**
 * Create a random generator that chooses increments bassd on a gaussian distribution.
 * @param {*} possibleIncrements 
 * @param {*} distances 
 */
/** 
 * TODO: Give me a better name
 * TODO: Add '@examples'
 */
function gaussianRandomIncrementGenerator(possibleIncrements, distances) {
    /**
     * @typedef {Object} Item
     * @property {number} [distance] (only exists when debugging)
     * @property {number} probability
     * @property {number} index
     */
    /**
     * Combined array of increment indices, distances and probabilities.
     * @type {Item[]}
     */
    const combinedArray = [];
    let totalProbability = 0;
    for (let i=0;i<possibleIncrements.length;i++) {
        const item = {index: i};
        if (debug.notes.soprano.gaussianDistribution) {
            item.distance = distances[i];
        }
        item.probability = gaussianDistribution.pdf(distances[i]);
        totalProbability += item.probability
        combinedArray[i] = item;
    }
    const probabilityMatrix = {};
    for (const item of combinedArray) {
        // normalize probabilities
        item.probability = item.probability / totalProbability;
        
        probabilityMatrix[item.index] = item.probability;
    }
    if (debug.notes.soprano.gaussianDistribution) {
        console.table(combinedArray.map(item => {
            // convert probabilities to human-friendly percent
            let copy = Object.assign({}, item);
            copy.probability = Math.round(copy.probability * 100);
            return copy;
        }));
    }
    return randomIncrementGenerator(probabilityMatrix);
}

/**
 * TODO: Describe me
 * @param {Song} song
 */
// TODO: Make this and generateBassNotes extend the same thing
function generateSopranoNotes({noteNames, stackedNotes: {bassNotes}, sopranoDistance}) {
    const notes = [];
    notes.push(bassNotes[0] + sopranoDistance);
    const possibleIncrements = [-8, -6, -5, -3, 0, 3, 5, 6, 8];
    const actualPossibleIncrements = [];
    for (const increment of possibleIncrements) {
        let actualIncrement = increment;
        if (increment < 0) {
            actualIncrement += 1;
        } else if (increment > 0) {
            actualIncrement -= 1;
        }
        actualPossibleIncrements.push(actualIncrement);
    }

    if (debug.notes.soprano.generation) {
        console.group('Soprano note generation');
    }
    
    for (let i=1;i<bassNotes.length-3;i++) {
        if (debug.notes.soprano.generation) {
            console.log('=============');
            console.log('position : ', i);
        }
        const bassNote = bassNotes[i];
        const lastSopranoNote = notes[i-1];
        let distancesToPossibleSopranoNotes = [];
        let highestDistance = 0;
        for (const actualIncrement of actualPossibleIncrements) {
            let distance = calculateSopranoNote(bassNote,actualIncrement,sopranoDistance) - lastSopranoNote;
            if (distance > highestDistance) {
                highestDistance = distance;
            }
            distancesToPossibleSopranoNotes.push(distance);
        }
        const sopranoRandomIncrementIndexGenerator = gaussianRandomIncrementGenerator(
            actualPossibleIncrements,
            distancesToPossibleSopranoNotes
        );
        const incrementIndex = sopranoRandomIncrementIndexGenerator();
        let currentSopranoNote = calculateSopranoNote(
            bassNote,
            actualPossibleIncrements[incrementIndex],
            sopranoDistance
        );
        if (debug.notes.soprano.generation) {
            console.log('distancesToPossibleSopranoNotes :', distancesToPossibleSopranoNotes);
            console.log('actualPossibleIncrements :', actualPossibleIncrements);
            console.log('incrementIndex :', incrementIndex);
            console.log('bassNote :', noteNames[bassNote]);
            console.log('increment :', actualPossibleIncrements[incrementIndex]);
            console.log('currentSopranoNote :', noteNames[currentSopranoNote], currentSopranoNote);
        }
        currentSopranoNote = correctTritone(noteNames, bassNote, currentSopranoNote);
        if (debug.notes.soprano.generation) {
            console.log('currentSopranoNote after tritone correction :', noteNames[currentSopranoNote], currentSopranoNote);
        }
        notes.push(currentSopranoNote);
    }
    notes.push(...writeCadence('soprano', notes));

    if (debug.notes.soprano.generation) {
        console.groupEnd();
    }

    return notes;
}

//#endregion

//#region Chords

/**
 * Adds a certain number of semitones to a note.
 * @param {string} note
 * @param {number} numberOfSemitones The number of semitones to add to the note.
 * @return {string}
 * @example
 * addSemitoneToNote('C', 3) // => Eb
 * addSemitoneToNote('C', 6) // => Gb
 * addSemitoneToNote('A', 3) // => C
 */
function addSemitoneToNote(note, numberOfSemitones) {
    const chroma = Tonal.Note.chroma(note);
    const newNote = chroma + numberOfSemitones;
    return Tonal.Note.pc(Tonal.Note.fromMidi(newNote));
}

/**
 * Gets chords from a key, root and type.
 * @param {SongKey} key 
 * @param {number[]} root // TODO: Describe me
 * @param {'M'|'m'} typeOfChord Whether to generate major or minor chords.
 * @return {Chord[]} List of notes in chord.
 * @example
 * buildChords('C', [0,5,7], 'M') // => [ 'CM', 'FM', 'GM' ]
 * buildChords('E', [2,4,9], 'm') // => [ 'Gbm', 'Abm', 'Dbm' ]
 */
function buildChords(key, root, typeOfChord) {
    const chords = [];
    for (const increment of root) {
        chords.push(addSemitoneToNote(key, increment) + typeOfChord);
    }
    return chords;
}

/**
 * Gets diatonic chords from a key.
 * @param {SongKey} key 
 * @returns {Chord[]} List of notes in chord.
 * @example
 * getAllDiatonicChords('C') // => [ 'CM', 'FM', 'GM', 'Dm', 'Em', 'Am' ]
 * getAllDiatonicChords('F') // => [ 'FM', 'BbM', 'CM', 'Gm', 'Am', 'Dm' ]
 */
function getAllDiatonicChords(key) {
    return [
        ...buildChords(key, [0,5,7], 'M'),
        ...buildChords(key, [2,4,9], 'm'),
    ];
}

/**
 * Gets all the possible chords some notes could belong to.
 * @param {SongKey} key 
 * @param {string[]} notes
 * @return {Chord[]} All the chords the notes could belong to.
 * @example
 * getPossibleChords('C', ['C', 'E', 'A']) // => [ 'Am' ]
 * getPossibleChords('C', ['C', 'F']) // => [ 'FM' ]
 * getPossibleChords('E', ['E', 'A']) // => [ 'AM' ]
 */
function getPossibleChords(key, notes) {
    const chords = getAllDiatonicChords(key);
    const matchingChords = [];
    for (const chord of chords) {
        const chordNotes = Tonal.Chord.notes(chord);
        let anyMissing = false;
        for (const note of notes) {
            if (!chordNotes.includes(note)) {
                anyMissing = true;
                break;
            }
        }
        if (!anyMissing) {
            matchingChords.push(chord);
        }
    }
    return matchingChords;
}

/**
 * From a list of chords, gets the chord with the largest distance to a reference chord.
 * @param {Chord[]} possibleChords List of chords to find one with a large interval to the reference chord..
 * @param {Chord} lastChord The reference chord.
 */
function getChordWithLongestInterval(possibleChords, lastChord) {
    let chosenChord = null;
    let largestInterval = 0;
    for (const possibleChord of possibleChords) {
        const possibleChordWithoutQuality = possibleChord.slice(0,possibleChord.length-1);
        // FIXME: This crashes when lastChord is null
        const lastChordWithoutQuality = lastChord.slice(0,lastChord.length - 1);
        
        let intervalString = Tonal.Distance.interval(possibleChordWithoutQuality, lastChordWithoutQuality);
        // remove letter from interval
        // FIXME: Handle no number in the interval
        let interval = Number(intervalString.match(/\d+/g, '')[0]);
        if (interval > 4) {
            interval = 9 - interval;
        }
        if (interval > largestInterval) {
            largestInterval = interval;
            chosenChord = possibleChord;
        }
        if (debug.chords) {
            console.log('chord, interval: ', possibleChord, interval);
        }
    }
    return chosenChord;
}

/**
 * @param {Song} song
 */
function generateProgressionChords({key, noteNames, totalNotes, stackedNotes: {bassNotes, sopranoNotes}}) {
    if (debug.chords) {
        console.log('');
        console.log('===progressionChords===');
        console.log('');
    }

    // NOTE: Change me if quality is changed
    const progressionChords = [key+'M'];
    for (let i=1;i<totalNotes;i++) {
        const bassNotePitchClass = Tonal.Note.pc(noteNames[bassNotes[i]]);
        const sopranoNotePitchClass = Tonal.Note.pc(noteNames[sopranoNotes[i]]);
        
        let chosenChord = null;
        const possibleChords = getPossibleChords(key, [bassNotePitchClass, sopranoNotePitchClass]);

        if (debug.chords) {
            console.log('========================');
            console.log('position : ', i);
            console.log('key :', key);
            console.log('bassNote :', bassNotePitchClass);
            console.log('sopranoNote :', sopranoNotePitchClass);
            console.log('possibleChords :', possibleChords);
        }

        // doubles the root
        if (bassNotePitchClass === sopranoNotePitchClass) {
            for (const chord of possibleChords) {
                if (chord.includes(bassNotePitchClass)) {
                    chosenChord = chord;
                    break;
                }
            }
        }

        if (chosenChord === null) {
            let lastProgressionChord = progressionChords[i-1];
            chosenChord = getChordWithLongestInterval(possibleChords, lastProgressionChord);

            if (debug.chords) {
                console.log('lastProgressionChord :', lastProgressionChord);
            }
        }

        if (debug.chords) {
            console.log('chosenChord :', chosenChord);
        }
        progressionChords.push(chosenChord);
    }
    if (debug.chords) {
        console.log('progressionChords :', progressionChords);
    }
    return progressionChords
}

//#endregion

//#region Third voice
/**
 * @param {Song} song
 */
function generateThirdVoice({noteNames, stackedNotes: {bassNotes, sopranoNotes}}) {
    const notes = [];
    for (let i=0;i<song.progressionChords.length;i++) {
        const chord = song.progressionChords[i];
        const notesInChord = Tonal.Chord.notes(chord);

        const bassNote = Tonal.Note.pc(noteNames[bassNotes[i]]);
        const sopranoNote = Tonal.Note.pc(noteNames[sopranoNotes[i]]);
        const otherNotes = notesInChord.filter(note => !(note.includes(bassNote) || note.includes(sopranoNote)));
        let thirdNote = null;
        if (otherNotes.length > 1) {
            let random = Math.random();
            if (random > 0.5) {
                thirdNote = otherNotes[0];
            } else {
                thirdNote = otherNotes[1];
            }
        } else {
            thirdNote = otherNotes[0];
        }
        let thirdNoteNumber = noteNames.indexOf(thirdNote+'4');
        notes.push(thirdNoteNumber);
    }
    return notes;
}
//#endregion

//#region Main program

function writeDataUriToFile(dataUri, path, filename) {
    const regex = /^data:.+\/(.+);base64,(.*)$/;
    const matches = dataUri.match(regex);
    const ext = matches[1];
    const data = matches[2];
    const buffer = new Buffer.from(data, 'base64');
    fs.writeFileSync(pathNode.join(path, filename+'.' + ext), buffer);
}

function createMidiTrack(notes, path='./dist', filename='song') {
    const track = new MidiWriter.Track();
    track.addEvent(new MidiWriter.ProgramChangeEvent({instrument :1}));
    track.addEvent(notes);
    // Generate a data URI
    const write = new MidiWriter.Writer([track]);
    writeDataUriToFile(write.dataUri(), path, filename);
}

function transposeNotes(totalNotes, notes) {
    const transposed = [];
    for (let i=0;i<totalNotes;i++) {
        transposed[i] = [];
    }

    let setNumber=0;
    for (const noteSet of Object.values(notes)) {
        for (let position=0;position<noteSet.length;position++) {
            transposed[position][setNumber] = noteSet[position];
        }
        setNumber++;
    }
    return transposed;
}

// TODO: Decide if this should just be in transpose notes for speed reasons
function noteNumbersToNames(noteNames, stackedNotes) {
    return stackedNotes.map(notes => notes.map(note => noteNames[note]));
}

/**
 * 
 * @param {Song} song
 * @param {string} noteDuration
 */
function generateNoteEvents({totalNotes, noteNames, stackedNotes: notesByVoice}, noteDuration) {
    const notesByPosition = transposeNotes(totalNotes, notesByVoice);
    const noteNamesByPosition = noteNumbersToNames(noteNames, notesByPosition);
    if (debug.notes.output) {
        console.table(noteNamesByPosition);
    }
    return noteNamesByPosition.map(noteSet => new MidiWriter.NoteEvent({pitch: noteSet, duration: noteDuration}));
}

let song = new Song('C', 7, 17, 7, '4');
song.generate();
song.createMidiTrack();

//#endregion